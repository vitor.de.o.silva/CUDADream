#include "mex.h"
#include "../src/dreamcirc_gpu.cu"
#include "../include/classes.cuh"

/*INPUT: pos_elem, raio, pos_trans, s_par, delay, m_par*/
/*OUTPUT: matriz de dimensoes nt x n_trans*/


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/* variable declarations here */
	int n_trans, n_elem;
	double raio;
	double dx, dy, dz, dt, nt;
	double *delay;
	double v, cp, alfa;

	/* code here */
	if (nrhs != 6)
		mexErrMsgIdAndTxt("dreamcirc_gpu:nrhs", "N�mero de inputs deve ser 6.");

	if (nlhs != 1) 
		mexErrMsgIdAndTxt("dreamcirc_gpu:nlhs", "N�mero de output deve ser 1.");

	if (mxGetN(prhs[0]) != 3)
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[0]:dimensoes",
			"Formato de posi��o dos transdutores errado. A matriz deve possuir tr�s colunas.");
	
	if (!mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]))
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[0]:tipo", "As posicoes dos transdutores devem ser do tipo Double.");

	n_trans = mxGetM(prhs[0]);

	if (!mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]) || mxGetNumberOfElements(prhs[1] != 1))
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[1]:tipo", "O raio deve ser do tipo Double.");

	raio = mxGetPr(prhs[1])[0];

	if (mxGetN(prhs[2]) != 3)
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[2]:dimensoes",
			"Formato de posi��o dos refletores errado. A matriz deve possuir tr�s colunas.");

	if (!mxIsDouble(prhs[2]) || mxIsComplex(prhs[2]))
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[2]:tipo", "As posicoes dos refletores devem ser do tipo Double.");

	n_elem = mxGetM(prhs[2]);

	if (mxGetM(prhs[3]) != 1 || !mxIsDouble(prhs[3]) || mxIsComplex(prhs[3] || mxGetN(prhs[3]) != 4))
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[3]", "s_par deve ser um vetor com quatro argumentos do tipo Double.");

	dx = mxGetPr(prhs[3])[0];
	dy = mxGetPr(prhs[3])[1];
	dz = mxGetPr(prhs[3])[2];
	dt = mxGetPr(prhs[3])[3];

	if (mxGetM(prhs[4]) != 1 || ((mxGetN(prhs[4]) != mxGetM(prhs[2]) && (mxGetN(prhs[4]) == 1))))
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[4]", "delay deve ser do tipo Double, sendo escalar ou vetor de dimens�o igual a quantidade de transdutores");

	delay = mxGetPr(prhs[4]);

	if (mxGetM(prhs[5]) != 1 || !mxIsDouble(prhs[5]) || mxIsComplex(prhs[5] || mxGetN(prhs[5]) != 3))
		mexErrMsgIdAndTxt("dreamcirc_gpu:prhs[5]", "m_par deve ser um vetor com tr�s argumentos do tipo Double.");

	v = mxGetPr(prhs[5])[0];
	cp = mxGetPr(prhs[5])[1];
	alfa = mxGetPr(prhs[5])[2];

	//criar matriz de saida
	plhs[0] = mxCreateDoubleMatrix(nt, n_trans,mxREAL);

	//transformar para a forma que o codigo cuda roda
	//executar codigo em gpu

	//transformar o valor de volta
	//escrever a saida
}