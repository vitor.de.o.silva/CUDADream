#ifndef H_FUNCOES
#define H_FUNCOES

__global__ void dreamcirc_gpu(DREAM dream, float* posTransdutor, int qtdeTransdutor, float* delay, int raio, float* posElemento, int qtdeElemento, float alfa, float* saida);
__device__ void dreamcirc_gpu_umElementoUmTransdutor(DREAM dream, float trans_x, float trans_y, float trans_z, float trans_delay, float trans_r, float elem_x, float elem_y, float elem_z, float alfa, float* saida);


#endif	//H_FUNCOES
