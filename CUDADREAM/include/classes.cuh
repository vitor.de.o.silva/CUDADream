#ifndef H_CLASSES
#define H_CLASSES

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

#define NTMAX 3000
#define NTHREADS 512

class DREAM
{
public:
	float dx, dy, dt;
	float v, cp, alfa;
	float fs, ts;
	int nt;

	DREAM();
	DREAM(float dx, float dy, float dt, int nt, float v, float cp, float alfa, float fs);
};

#endif // H_CLASSES