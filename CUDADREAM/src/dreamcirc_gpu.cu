#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <math.h>
#include <cstdlib>
#include "../include/classes.cuh"
#include "../include/funcoes.cuh"

void dreamcirc_gpu(DREAM dream, float* posTransdutor_h, int qtdeTransdutor, float* delay_h, int raio, float* posElemento_h, int qtdeElemento, float alfa)
{
	float* posTransdutor_d, *delay_d, *posElemento_d, *saida_d;
	float* saida_h;

	saida_h = (float*)malloc(sizeof(float)*qtdeTransdutor*dream.nt);
	cudaMalloc((void**)&saida_d, sizeof(float)*qtdeTransdutor*dream.nt);

	/*COPIANDO PARA GPU*/
	/*posicoes dos transdutores*/
	cudaMalloc((void**)&posTransdutor_d, sizeof(float)*qtdeTransdutor * 3);
	cudaMemcpy(posTransdutor_d, posTransdutor_h, sizeof(float)*qtdeTransdutor * 3, cudaMemcpyHostToDevice);

	/*delay dos transdutores*/
	cudaMalloc((void**)&delay_d, sizeof(float)*qtdeTransdutor);
	cudaMemcpy(delay_d, delay_h, sizeof(float)*qtdeTransdutor, cudaMemcpyHostToDevice);

	/*posicoes dos elementos*/
	cudaMalloc((void**)&posElemento_d, sizeof(float)*qtdeElemento * 3);
	cudaMemcpy(posElemento_d, posElemento_h, sizeof(float)*qtdeElemento * 3, cudaMemcpyHostToDevice);

	dreamcirc_gpu<<<((NTHREADS + qtdeTransdutor-1)/NTHREADS, NTHREADS>>>(dream, posTransdutor_d, qtdeTransdutor, delay_d, raio, posElemento_d, qtdeElemento, alfa, saida_d);

	cudaMemcpy(&saida_h, saida_d, sizeof(float)*qtdeTransdutor*dream.nt, cudaMemcpyDeviceToHost);

	cudaFree(saida_d);
	cudaFree(posTransdutor_d);
	cudaFree(delay_d);
	cudaFree(posElemento_d);
}


/*CALCULA A SIR DE UM CONJUNTO DE TRANSDUTORES*/
__global__ void dreamcirc_gpu(DREAM dream, float* posTransdutor, int qtdeTransdutor, float* delay, int raio, float* posElemento, int qtdeElemento, float alfa, float* saida)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;

	__shared__ float resultado[NTMAX][NTHREADS];

	/*ZERA A MATRIZ DE SAIDA*/
	if (index < dream.nt)
		for (int i = 0; i < qtdeTransdutor; i++)
			saida[i*qtdeTransdutor + index] = 0;

	/*ZERA O VETOR RESULTADO*/
	if (index < NTMAX)
		for (int i = 0; i < NTHREADS; i++)
			resultado[index][i] = 0;

	/*CADA THREAD CALCULA O SINAL DE UM ELEMENTO REFLETOR*/
	/*CADA ITERACAO CALCULA O SINAL RECEBIDO POR UM TRANSDUTOR*/
	for (int i = 0; i < qtdeTransdutor; i++)
	{
		if (index < qtdeElemento)
			dreamcirc_gpu_umElementoUmTransdutor(dream, posTransdutor[i * 3], posTransdutor[i * 3 + 1], posTransdutor[i * 3 + 2], delay[i], raio, posElemento[index * 3], posElemento[index * 3 + 1], posElemento[index * 3 + 2], alfa, &resultado[0][index]);
		if (index < dream.nt)
			for (int j = 0; j < qtdeElemento; j++)
				saida[i*dream.nt + index] += resultado[index][j];
	}

	return;
}

__device__ void dreamcirc_gpu_umElementoUmTransdutor(DREAM dream, float trans_x, float trans_y, float trans_z, float trans_delay, float trans_r, float elem_x, float elem_y, float elem_z, float alfa, float* saida)
{
	/*CODIGO ADAPTADO DE https://sourceforge.net/p/dreamtoolbox/code/HEAD/tree/trunk/src/dreamcirc.c */

	float x_min = 0;
	float x_max = 0;
	float x_atual = 0;
	float x_var = 0;

	float y_min = 0;
	float y_max = 0;
	float y_atual = 0;

	float dist_x = 0;
	float dist_y = 0;
	float dist_z = 0;
	float dist = 0;

	int t = 0;

	float impulso = 0;

	float pi = (float)atan((double) 1.0) * 4.0;
	float ds = dream.dx * dream.dy;

	dist_z = elem_z - trans_z;				//todo transdutor est� em z=0;
	
	y_min = trans_y -trans_r;
	y_max = trans_y + trans_r;
	y_atual = y_min + dream.dy / 2.0;

	while (y_atual <= y_max)
	{
		x_var = sqrt(trans_r*trans_r - (trans_y - y_atual)*(trans_y - y_atual));
		x_min = trans_x - x_var;
		x_max = trans_x - x_var;

		dist_y = elem_y - y_atual;

		x_atual = x_min + dream.dx / 2.0;
		while (x_atual <= x_max)
		{
			dist_x = elem_x - x_atual;
			dist = sqrt(dist_x*dist_x + dist_y*dist_y + dist_z*dist_z);

			impulso = dream.v * ds / (2 * pi * dist * dream.dt);
			impulso *= 1000;		//converte p/ unidades do SI

			t = (int)((dist * 1000 / dream.cp)-trans_delay);

			if ((t < dream.nt) && (t >= 0))
			{
				if (alfa == (float)0.0)
					saida[t] = impulso;
				else
					/*att(alfa, dist, t, dream.dt, dream.cp, saida, dream.nt, impulso)*/;
			}

			else
				/*SIR OUT OF BOUNDS*/;

			x_atual += dream.dx;
		}

		y_atual += dream.dy;
	}
}