#include "../include/classes.cuh"

DREAM::DREAM() {};

DREAM::DREAM(float dx, float dy, float dt, int nt, float v, float cp, float alfa, float fs)
{
	if (nt > NTMAX)
	{
		printf("ERRO: nt maximo: %d\n", NTMAX);
		return;
	}

	DREAM::dx = dx;
	DREAM::dy = dy;
	DREAM::dt = dt;
	DREAM::nt = nt;
	DREAM::v = v;
	DREAM::cp = cp;
	DREAM::alfa = alfa;
	DREAM::fs = fs;
	DREAM::ts = 1 / fs;
}